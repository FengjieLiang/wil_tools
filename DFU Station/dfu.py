from Crypto.Cipher import AES
from bluepy import btle
from bluepy.btle import Scanner, DefaultDelegate
import struct
import time
import os
import csv
import sys, getopt

wil_dfu_module = 'ed:b5:db:9b:aa:d8'
CTC_Company_ID = "7607"

aes_service_uuid = '5c690030-313a-45c7-a72e-ee9238daeb03'
aes_write_characteristic_uuid = '5c690031-313a-45c7-a72e-ee9238daeb03'
aes_read_characteristic_uuid = '5c690032-313a-45c7-a72e-ee9238daeb03'

buttonless_dfu_service_uuid = '0000fe59-0000-1000-8000-00805f9b34fb'
buttonless_dfu_characteristic_uuid = '8ec90003-f315-4f60-9fb8-838830daea50'

device_info_service_uuid = '0000180a-0000-1000-8000-00805f9b34fb'
firmware_revision_characteristic_uuid = '00002a26-0000-1000-8000-00805f9b34fb'

global conn
global nonce
wil_module_set = set()
dfu_result = dict()
firmware_path = ""


class ScanDelegate(DefaultDelegate):
    def __init__(self):
        DefaultDelegate.__init__(self)

    def handleDiscovery(self, dev, isNewDev, isNewData):
        if isNewDev:
            for (adtype, desc, value) in dev.getScanData():
                if adtype == 0xff:
                    company_id = value[0: 4]
                    if company_id == CTC_Company_ID:
                        print("")
                        print('*****-- Device Address: %s' % dev.addr)
                        wil_module_set.add(dev.addr)
        elif isNewData:
            pass


def scan_wil_module():
    scanner = Scanner().withDelegate(ScanDelegate())
    i = 0
    while True:
        try:
            scanner.scan(2.0)
            break
        except:
            pass
            i = i + 1
            print("!!!!!!scan error!!!")
            if i == 10:
                break

    print("")
    print("--------------------------------------------------------------------")
    print("scan finish, show the device list")
    print(wil_module_set)


class MyDelegate(btle.DefaultDelegate):
    def __init__(self):
        btle.DefaultDelegate.__init__(self)

    def handleNotification(self, cHandle, data):
        pass


def dfu_connect(bt_addr):
    global conn
    i = 0
    flag = 0
    while True:
        try:
            print("wil_connect")

            conn = btle.Peripheral(bt_addr, btle.ADDR_TYPE_RANDOM)
            conn.setMTU(243)
            conn.setDelegate(MyDelegate())
            flag = 1
            break
        except:
            i = i + 1
            print("!!!wil connect error!!!")
            print("!!!try again!!!")
            if i == 10:
                flag = 0
                break
    if flag == 1:
        read_nonce_from_aes_service()
        write_ciphertext_in_aes_service()
        time.sleep(1)
        return True
    else:
        return False


def read_nonce_from_aes_service():
    aes_service = conn.getServiceByUUID(aes_service_uuid)
    aes_characteristic = aes_service.getCharacteristics()

    for charac in aes_characteristic:
        if aes_read_characteristic_uuid == charac.uuid:
            global nonce
            nonce = charac.read()
            # print(trans(nonce))


def write_ciphertext_in_aes_service():
    key = rb'BerisConsultingX'
    global nonce
    cipher = AES.new(nonce, AES.MODE_ECB)
    ciphertext = cipher.encrypt(key)

    aes_service = conn.getServiceByUUID(aes_service_uuid)
    aes_characteristic = aes_service.getCharacteristics()

    for charac in aes_characteristic:
        if aes_write_characteristic_uuid == charac.uuid:
            charac.write(ciphertext)


def read_firmware_version_from_module():
    device_info_service = conn.getServiceByUUID(device_info_service_uuid)
    device_info_characteristic = device_info_service.getCharacteristics()

    for charac in device_info_characteristic:
        if charac.uuid == firmware_revision_characteristic_uuid:
            old_firmware = str(charac.read(), encoding="utf-8")
            print("read firmware version: ", old_firmware)
            dfu_result["old firmware"] = old_firmware


def enable_dfu_notification():
    global conn
    notif_enable = [0x02, 0x00]
    conn.writeCharacteristic(31, bytes(notif_enable), False)


def write_dfu_request(data):
    global conn
    buttonless_dfu_service = conn.getServiceByUUID(buttonless_dfu_service_uuid)
    buttonless_dfu_characteristic = buttonless_dfu_service.getCharacteristics()
    for charac in buttonless_dfu_characteristic:
        if buttonless_dfu_characteristic_uuid == charac.uuid:
            charac.write(data, True)
            print("write dfu request successfully")


def enter_dfu_mode(addr):
    # dfu_connect(addr)
    enable_dfu_notification()
    print("enable dfu notification")
    time.sleep(1)
    print("")
    print("now enter DFU Mode")
    print("")
    dfu_command = [0x01]
    write_dfu_request(bytes(dfu_command))
    time.sleep(1)
    # conn.disconnect()


def get_dfu_addr(addr):
    str_last_addr = addr[-2:]
    int_last_addr = int(str_last_addr, 16)
    if int_last_addr < 0xff:
        int_dfu_last_addr = int_last_addr + 0x01
    else:
        int_dfu_last_addr = 0x00
    hex_dfu_last_addr = hex(int_dfu_last_addr)

    str_dfu_last_addr = str(hex_dfu_last_addr).replace("0x", "")
    str_dfu_last_addr = str_dfu_last_addr.zfill(2)
    str_dfu_last_addr = str_dfu_last_addr.upper()
    dfu_addr = addr[0:-2] + str_dfu_last_addr
    return dfu_addr


def wil_dfu_process(addr):
    print("")
    print("now update firmware for Module: " + addr)
    dfu_result["BT address"] = addr

    if dfu_connect(addr):
        read_firmware_version_from_module()
        enter_dfu_mode(addr)
        dfu_addr = get_dfu_addr(addr)
        # command = "./nrfdfu -b " + dfu_addr + " -p /home/pi/DFU_script/WIL_B_1bb61b6c.zip"
        command = "./nrfdfu -b " + dfu_addr + " -p " + firmware_path
        print(dfu_addr)
        print(command)
        print("now updating...")
        print("")
        print("")
        time.sleep(5)
        try_count = 0
        while True:
            try:
                dfu_status = os.system(command)
            except:
                print("run dfu command failed")
                dfu_status = -1
            if dfu_status == 0:
                dfu_result["status"] = "pass"
                break
            else:
                dfu_result["status"] = "fail"
                try_count = try_count + 1
                time.sleep(2)
                if try_count == 5:
                    break
    else:
        dfu_result["status"] = "fail"


def show_fw_files():
    path = "/home/pi/wil_dfu_firmware"
    all_files = os.listdir(path)
    firmware_list = list()
    print("")
    print("please enter the number to choose the firmware ")
    print("")
    for i in all_files:
        if os.path.splitext(i)[1] == ".zip":
            firmware_list.append(i)
    while True:
        index = 1
        rescan_flag = False
        for fw in firmware_list:
            print("%d  %s" % (index, fw))
            index = index + 1
        print("%d  rescan" % index)
        print("")
        print("If the firmware is not found, place the firmware in the /home/pi/wil_dfu_firmware folder, and rescan.")
        while True:
            in_str = input("please enter: ")
            try:
                in_int = int(in_str)
            except:
                print("please enter a number")
                continue
            if index > in_int > 0:
                global firmware_path
                filename_firmware = firmware_list[in_int - 1]
                print("Your selected firmware: " + filename_firmware)
                firmware_path = path + "/" + filename_firmware
                rescan_flag = False
                dfu_result["new firmware"] = filename_firmware[-12:-4]
                break
            elif in_int == index:
                rescan_flag = True
                break
            else:
                print("please enter a number!")
                continue
        if rescan_flag:
            pass
        else:
            break


def read_device_list_from_csv(csv_path):
    print("read device list from csv file")
    csvfile = open(csv_path, "r")
    reader = csv.reader(csvfile)

    global wil_module_set
    for item in reader:
        wil_module_set.add(item[0])

    csvfile.close()


def write_dfu_result():
    fileHeader = ["BT address", "old firmware", "new firmware", "status", "date"]

    date = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
    dfu_result["date"] = date
    if os.path.exists("dfu_result.csv"):
        csvfile = open("dfu_result.csv", "a+")
        writer = csv.writer(csvfile)
    else:
        csvfile = open("dfu_result.csv", "a+")
        writer = csv.writer(csvfile)
        writer.writerow(fileHeader)
    result = [dfu_result["BT address"], dfu_result["old firmware"], dfu_result["new firmware"], dfu_result["status"],
              dfu_result["date"]]

    writer.writerow(result)


def wil_dfu(fw_path, csv_path):
    read_device_list_from_csv(csv_path)
    global firmware_path
    firmware_path = fw_path
    for dev in wil_module_set:
        try:
            wil_dfu_process(dev)
            write_dfu_result()
            time.sleep(2)
        except:
            continue


def main():
    firmware_file = ""
    device_list_csv = ""
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hf:d:", ["help", "firmware=", "device="])
    except getopt.GetoptError:
        print("dfu.py -f <firmware file> -d <device list CSV file>")
        sys.exit(2)

    if not opts:
        print("Usage: python3 dfu.py -f firmware -d device_list")
        exit()
    else:
        for opt, arg in opts:
            if opt in ("-h", "--help"):
                print('dfu.py -f <firmware file> -d <device list CSV file>')
                sys.exit()
            elif opt in ("-f", "--firmware"):
                firmware_file = arg
            elif opt in ("-d", "--device"):
                device_list_csv = arg
    curdir = os.path.abspath(os.curdir)
    firmware_file_path = curdir + "/" + firmware_file
    device_list_csv_path = curdir + "/" + device_list_csv

    if os.path.exists(firmware_file_path) and os.path.exists(device_list_csv_path):
        dfu_result["new firmware"] = firmware_file[-12:-4]
        wil_dfu(firmware_file_path, device_list_csv_path)
    elif not os.path.exists(firmware_file_path):
        print("firmware file not exist")
        print("firmware file path: ", firmware_file_path)
        sys.exit()
    elif not os.path.exists(device_list_csv_path):
        print("device list csv file not exist")
        print("device list csv file path: ", device_list_csv_path)
        sys.exit()


if __name__ == '__main__':
    main()
