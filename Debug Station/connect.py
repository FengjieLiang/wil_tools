from bluepy import btle
import struct
import time
from test_setting import temp_min, temp_max, acceleration, temp_min_enable, \
    temp_max_enable, acceleration_enable, light_enable
from draw import read_config_file, read_start_time
from Crypto.Cipher import AES

# from Crypto.Util.Padding import pad

# from draw import read_config_file, read_start_time

sensor_config_command = []

universal_service_uuid = '5c690020-313a-45c7-a72e-ee9238daeb03'
universal_characteristic_uuid = '5c690021-313a-45c7-a72e-ee9238daeb03'

aes_service_uuid = '5c690030-313a-45c7-a72e-ee9238daeb03'
aes_write_characteristic_uuid = '5c690031-313a-45c7-a72e-ee9238daeb03'
aes_read_characteristic_uuid = '5c690032-313a-45c7-a72e-ee9238daeb03'

temperature_counter = 0

# notif type 0->sensor config notification
#            1->tour log notification
notification_type = 0
notification_counter = 0

log_all_notification = []

global notification_read_all
global timeout_counter

global conn
global nonce

# conn = btle.Peripheral()

wil_device_addr = ""
# global total_length, actual_length, flag_transmission_not_complete


total_length = 0
actual_length = 0
flag_transmission_not_complete = False
transmission_time = 0
log_counter = 0
counter_difference = 0
counter_difference_old = 0
lost_packet_index = []


class MyDelegate(btle.DefaultDelegate):
    def __init__(self):
        btle.DefaultDelegate.__init__(self)

    def handleNotification(self, cHandle, data):
        global timeout_counter
        timeout_counter = 10
        if 0 == notification_type:
            # sensor config notification
            notif_data = []
            # convent bytearray to list
            for i in data:
                notif_data.append(i)
            save_config_file(wil_device_addr, wil_aes_decrypt(notif_data))
        else:
            # wil log
            # global log_one_notification
            log_one_notification_cipher = []
            for i in data:
                log_one_notification_cipher.append(i)
                # print(log_one_notification)
            log_one_notification = wil_aes_decrypt(log_one_notification_cipher)
            # print("the length of log_one_notification =%d" % (len(log_one_notification)))

            global log_all_notification
            log_all_notification.append(log_one_notification)
            # print(log_all_notification)
            global notification_counter
            notification_counter = notification_counter + 1

            global notification_read_all
            if wil_log_read_complete(notification_counter, log_one_notification):
                notification_read_all = True
            else:
                notification_read_all = False


def wil_connect(bt_addr):
    global conn
    # global ble_addr_wil_modul1
    i = 0
    while True:
        try:
            print("wil_connect")

            conn = btle.Peripheral(bt_addr, btle.ADDR_TYPE_RANDOM)
            conn.setMTU(243)
            conn.setDelegate(MyDelegate())
            break
        except:
            i = i + 1
            print("!!!wil connect error!!!")
            print("!!!try again!!!")
            if i == 10:
                break

    read_nonce_from_aes_service()
    write_ciphertext_in_aes_service()

    enable_notification()
    print("enable notification")
    global notification_counter
    notification_counter = 0
    global temperature_counter
    temperature_counter = 0

    # pick_by_light(10,1)
    # print(conn)
    '''service_dic=conn.getServices()
    for service in service_dic:
        print(service.uuid)'''

    '''universal_service=conn.getServiceByUUID(universal_service_uuid)
    print(universal_service)
    universal_characteristic=universal_service.getCharacteristics()
    print(universal_characteristic)
    for charac in universal_characteristic:
        print(charac)'''


def wil_disconnect():
    global conn
    conn.disconnect()
    global notification_counter
    notification_counter = 0
    global temperature_counter
    temperature_counter = 0


def enable_notification():
    global conn
    notif_enable = [0x01, 0x00]
    conn.writeCharacteristic(27, bytes(notif_enable), False)


def disable_notification():
    global conn
    notif_disable = [0x00, 0x00]
    conn.writeCharacteristic(27, bytes(notif_disable), False)


# def trans(s):
#     return "b'%s'" % ''.join('\\x%.2x' % x for x in s)


def read_nonce_from_aes_service():
    aes_service = conn.getServiceByUUID(aes_service_uuid)
    aes_characteristic = aes_service.getCharacteristics()

    for charac in aes_characteristic:
        if aes_read_characteristic_uuid == charac.uuid:
            global nonce
            nonce = charac.read()
            # print(trans(nonce))


def write_ciphertext_in_aes_service():
    key = rb'BerisConsultingX'
    global nonce
    cipher = AES.new(nonce, AES.MODE_ECB)
    ciphertext = cipher.encrypt(key)

    aes_service = conn.getServiceByUUID(aes_service_uuid)
    aes_characteristic = aes_service.getCharacteristics()

    for charac in aes_characteristic:
        if aes_write_characteristic_uuid == charac.uuid:
            charac.write(ciphertext)


def wil_aes_encrypt(data):
    global nonce
    cipher = AES.new(nonce, AES.MODE_ECB)
    ciphertext = cipher.encrypt(bytes(data).ljust(16, b'0'))
    return ciphertext


def wil_aes_decrypt(ciphertext):
    global nonce
    cipher = AES.new(nonce, AES.MODE_ECB)
    cleartext = cipher.decrypt(bytes(ciphertext))
    return list(cleartext)


def write_universal_characteristic(data):
    global conn
    universal_service = conn.getServiceByUUID(universal_service_uuid)
    universal_characteristic = universal_service.getCharacteristics()

    for charac in universal_characteristic:
        if universal_characteristic_uuid == charac.uuid:
            charac.write(wil_aes_encrypt(data))


def pick_by_light(num, mode):
    pick_by_light_command = [0xa2, num, mode]
    write_universal_characteristic(pick_by_light_command)


def sensor_config_data_prepare(temp_intervall):
    global sensor_config_command
    sensor_config_command.clear()
    sensor_config_command.append(0xa1)
    accel_bytes = struct.pack('f', acceleration)
    sensor_config_command.append(temp_min)
    sensor_config_command.append(temp_max)
    sensor_config_command.append(temp_intervall)
    sensor_config_command.append(accel_bytes[3])
    sensor_config_command.append(accel_bytes[2])
    sensor_config_command.append(accel_bytes[1])
    sensor_config_command.append(accel_bytes[0])

    sensor_activate = 0

    if temp_min_enable == 1:
        sensor_activate = sensor_activate | (1 << 0)
    else:
        sensor_activate = sensor_activate | (0 << 0)

    if temp_max_enable == 1:
        sensor_activate = sensor_activate | (1 << 1)
    else:
        sensor_activate = sensor_activate | (0 << 1)

    if acceleration_enable == 1:
        sensor_activate = sensor_activate | (1 << 2)
    else:
        sensor_activate = sensor_activate | (0 << 2)

    if light_enable == 1:
        sensor_activate = sensor_activate | (1 << 3)
    else:
        sensor_activate = sensor_activate | (0 << 4)

    sensor_config_command.append(sensor_activate)


def wil_sensor_config(temp_intervall):
    sensor_config_data_prepare(temp_intervall)
    global sensor_config_command
    write_universal_characteristic(sensor_config_command)
    print(sensor_config_command)


def wil_start_tour(bt_addr):
    local_time = time.time()
    local_time_int = int(local_time)
    local_time_bytes = local_time_int.to_bytes(4, byteorder="big", signed=True)

    # print("------------wil_start_tour ------%d" % local_time_int)
    time_local = time.localtime(local_time_int)
    dt = time.strftime("------- wil_start_tour ------ %Y-%m-%d %H:%M:%S", time_local)
    dt_for_write = time.strftime("%Y-%m-%d %H:%M:%S", time_local)
    print(dt)

    tour_start_command = [0xb1, local_time_bytes[0], local_time_bytes[1], local_time_bytes[2], local_time_bytes[3],
                          local_time_bytes[0], local_time_bytes[1], local_time_bytes[2], local_time_bytes[3]]

    write_universal_characteristic(tour_start_command)

    file_name = bt_addr.replace(':', '_') + "_tour_log.txt"
    file_path = "/home/pi/Debug_Test/tour_log/" + file_name
    tour_logfile = open(file_path, "a+")
    write_str = "start time:" + "\t" + dt_for_write + "\t"
    tour_logfile.write(write_str)
    tour_logfile.close()


def wil_stop_tour(bt_addr):
    local_time = time.time()
    local_time_int = int(local_time)
    local_time_bytes = local_time_int.to_bytes(4, byteorder="little", signed=True)

    time_local = time.localtime(local_time_int)
    dt = time.strftime("------- wil_stop_tour ------ %Y-%m-%d %H:%M:%S", time_local)
    dt_for_write = time.strftime("%Y-%m-%d %H:%M:%S", time_local)
    print(dt)

    tour_stop_command = [0xb2, local_time_bytes[0], local_time_bytes[1], local_time_bytes[2], local_time_bytes[3]]
    write_universal_characteristic(tour_stop_command)

    file_name = bt_addr.replace(':', '_') + "_tour_log.txt"
    file_path = "/home/pi/Debug_Test/tour_log/" + file_name
    tour_logfile = open(file_path, "a+")
    write_str = "stop time:" + "\t" + dt_for_write + "\r\n"
    tour_logfile.write(write_str)
    tour_logfile.close()


def wil_read_sensor_config(bt_addr):
    global wil_device_addr
    wil_device_addr = bt_addr
    global conn
    conn.setDelegate(MyDelegate())
    global notification_type
    notification_type = 0
    read_config_command = [0xc1]
    write_universal_characteristic(read_config_command)

    if conn.waitForNotifications(2.0):
        # print("Notification")
        pass


def wil_read_log(bt_addr):
    global conn
    conn.setDelegate(MyDelegate())

    global flag_transmission_not_complete, actual_length, total_length
    total_length = 0
    actual_length = 0
    flag_transmission_not_complete = False

    global notification_type
    notification_type = 1

    transmission_start_time = time.time()
    read_log_command = [0xc2]
    write_universal_characteristic(read_log_command)
    global notification_read_all
    notification_read_all = False
    while True:
        global timeout_counter
        if not notification_read_all and conn.waitForNotifications(1):
            continue
        if notification_read_all:
            break
        if timeout_counter == 0:
            print("receive Notification timeout!!!")
            # global flag_transmission_not_complete
            flag_transmission_not_complete = True
            break
        print("timeout_counter=%d" % timeout_counter)
        timeout_counter -= 1

    transmission_stop_time = time.time()
    global transmission_time
    transmission_time = transmission_stop_time - transmission_start_time
    print("total notification=%d" % notification_counter)
    # global  log_all_notification

    print("---------------------------------------------------------")
    print("print log_all_notification")
    print_all_notification_in_hex(log_all_notification)
    print("---------------------------------------------------------")

    notif_counter = 0
    for notif in log_all_notification:
        if notif_counter == notification_counter:
            print("notif_counter==notification_counter")
            break
        elif notif_counter == 0:
            wil_header_handle(notif)
        else:
            wil_log_handle(bt_addr, notif)
            # print("notif_counter=%d" % notif_counter)
        notif_counter = notif_counter + 1
    log_all_notification.clear()
    return True


def print_all_notification_in_hex(notification_list):
    for notification in notification_list:
        counter = 1
        print("[", end="")
        for element in notification:
            print("%x" % element, end="")
            if counter < len(notification):
                print(", ", end="")
            else:
                print("]")
            counter += 1


def wil_header_handle(notification_data):
    total_length_list = notification_data[:4]
    total_length_bytearray = bytearray(total_length_list)
    total_length_tuple = struct.unpack('<i', total_length_bytearray)
    global total_length
    total_length = total_length_tuple[0]
    print("notification total effective length=%d" % total_length)


# return True--->stop tour, stop get notification
# return False--->getting notification
def wil_log_read_complete(counter, data):
    if counter == 1:  # header
        total_length_list = data[:4]
        total_length_bytearray = bytearray(total_length_list)
        total_length_tuple = struct.unpack('<i', total_length_bytearray)
        total_length = total_length_tuple[0]
        print("total_length=%d" % total_length)
        if total_length == 0:
            return True
        else:
            return False
    else:  # logs
        current_notif_length = data[4]
        read_pos = 5
        while True:
            if read_pos == 5 + current_notif_length:
                break
            elif data[read_pos] == 0xe0:
                event_num = data[read_pos + 1]
                if event_num == 1:
                    # print("---Event---start tour")
                    pass
                elif event_num == 2:
                    print("---Event---stop tour")
                    print("stop receiving notifications")
                    return True
                read_pos = read_pos + 6
            elif data[read_pos] == 0xe2:
                read_pos = read_pos + 2
            elif data[read_pos] == 0xe3:
                read_pos = read_pos + 3
            elif data[read_pos] == 0xe4:
                read_pos = read_pos + 10
            elif data[read_pos] == 0xe5:
                read_pos = read_pos + 7
            elif data[read_pos] == 0xe6:
                read_pos = read_pos + 6
            elif data[read_pos] == 0xe7:
                read_pos = read_pos + 9
            elif data[read_pos] == 0xe8:
                read_pos = read_pos + 3
            elif data[read_pos] == 0xff:
                break
        return False


def wil_log_handle(bt_addr, notification_data):
    global log_counter, counter_difference, counter_difference_old, lost_packet_index
    notif_num_list = notification_data[:4]
    notif_num_bytearray = bytearray(notif_num_list)
    notif_num_tuple = struct.unpack('<i', notif_num_bytearray)
    notif_num = notif_num_tuple[0]

    counter_difference = notif_num - log_counter
    if counter_difference > counter_difference_old:
        for i in range(counter_difference - counter_difference_old):
            print("lost packet index=%d" % (log_counter + counter_difference_old + i))
            lost_packet_index.append(log_counter + counter_difference_old + i)
        counter_difference_old = counter_difference
    log_counter += 1

    current_notif_length = notification_data[4]
    # print("current_notif_length=%d  " % current_notif_length)
    global actual_length
    actual_length += current_notif_length
    read_pos = 5
    global temperature_counter
    while True:
        if read_pos == 5 + current_notif_length:
            break
        # print("read_pos=%d" % read_pos)
        # print("notification_data[%d]=0x%x" % (read_pos, notification_data[read_pos]))
        if notification_data[read_pos] == 0xe0:
            event_num = notification_data[read_pos + 1]
            time_list = notification_data[read_pos + 2:read_pos + 6]
            # print("time_list  ", end="")
            # print(time_list)
            time_bytearray = bytearray(time_list)
            time_tuple = struct.unpack('<i', time_bytearray)
            time_int = time_tuple[0]
            # print("time_int=%d" % time_int)

            time_local = time.localtime(time_int)
            dt = time.strftime("------------wil_log_handle ------%Y-%m-%d %H:%M:%S", time_local)
            print(dt)

            event = 0
            global notification_read_all
            if event_num == 1:
                print("---Event---start tour")
                event = 1
            elif event_num == 2:
                print("---Event---stop tour")
                event = 2
                notification_read_all = True

            show_time(bt_addr, event, time_int)

            read_pos = read_pos + 6

        elif notification_data[read_pos] == 0xe3:
            high_byte = notification_data[read_pos + 2]
            low_byte = notification_data[read_pos + 1]
            temperature = temperature_conversion_B2F(high_byte, low_byte)
            # print("temperature=%f" % temperature)
            # save temperature in file
            # global temperature_counter
            temperature_counter = temperature_counter + 1
            save_temperature_file(bt_addr, temperature_counter, temperature)
            read_pos = read_pos + 3

        elif notification_data[read_pos] == 0xe5:
            accel_integer = notification_data[read_pos + 1]
            accel_decimal = notification_data[read_pos + 2]
            accel = accel_integer + accel_decimal / 100

            timestamp_accel = notification_data[read_pos + 6] << 24 | notification_data[read_pos + 5] << 16 | \
                              notification_data[read_pos + 4] << 8 | notification_data[read_pos + 3]
            time_local_accel = time.localtime(timestamp_accel)
            dt_accel = time.strftime("%Y-%m-%d %H:%M:%S", time_local_accel)

            temperature_counter = temperature_counter + 1
            save_accel_file(bt_addr, temperature_counter, dt_accel, accel)
            read_pos = read_pos + 7

        elif notification_data[read_pos] == 0xe6:
            light_code = notification_data[read_pos + 1]
            if light_code == 1:
                box_state = "opened"
            elif light_code == 2:
                box_state = "closed"
            elif light_code == 0:
                box_state = "undefined"
            else:
                box_state = "error, light code is undefined"
            timestamp_light = notification_data[read_pos + 5] << 24 | notification_data[read_pos + 4] << 16 | \
                              notification_data[read_pos + 3] << 8 | notification_data[read_pos + 2]
            time_local_light = time.localtime(timestamp_light)
            dt_light = time.strftime("%Y-%m-%d %H:%M:%S", time_local_light)

            temperature_counter = temperature_counter + 1
            save_light_file(bt_addr, temperature_counter, dt_light, box_state)
            read_pos = read_pos + 6

        elif notification_data[read_pos] == 0xe7:
            version_list = list()
            version_str = ""
            for i in range(8):
                version_list.append(notification_data[read_pos + i + 1])
                version_str = version_str + chr(version_list[i])
            save_version_file(bt_addr, version_str)
            read_pos = read_pos + 9

        elif notification_data[read_pos] == 0xe8:
            read_pos = read_pos + 3

        elif notification_data[read_pos] == 0xff:
            break
        else:
            print("!!!wil log error!!!")
            print("notification data:")
            print(notification_data)
            break
            # while True:
            #     pass


def temperature_conversion_B2F(high_byte, low_byte):
    temp_16b = (high_byte << 4) | (low_byte >> 4)
    if (temp_16b & 0x800) == 0x800:
        # temperatur<0
        temp_16b = ~(temp_16b - 1)
        temp_16b = temp_16b & 0x0fff
        f_temperature = temp_16b * -0.0625
    else:
        f_temperature = temp_16b * 0.0625
    return f_temperature


def show_time(bt_addr, event, module_time):
    print("show time")
    print("event code =%d" % event)
    time_local = time.localtime(module_time)
    dt = time.strftime("%Y-%m-%d %H:%M:%S", time_local)
    if event == 1:
        # start tour
        print("start time:", end="")
        time_file = "/home/pi/Debug_Test/tour_log/" + bt_addr.replace(':', '_') + "_start_time_file.txt"
        tf = open(time_file, "w")
        tf.write(str(module_time))
        tf.close()

    elif event == 2:
        # stop tour
        # print("stop time:", end="")
        # time_file = "/home/pi/Debug_Test/stop_time_file.txt"
        # tf = open(time_file, "w")
        # tf.write(str(module_time))
        # tf.close()
        pass

    else:
        print("wrong event code")
    print(dt)


def save_temperature_file(bt_addr, counter, temp_value):
    wil_config = read_config_file(bt_addr)
    mesurement_intervall = wil_config[2]

    stat_time_int = read_start_time(bt_addr)
    time_int = stat_time_int + mesurement_intervall * counter
    time_local = time.localtime(time_int)
    dates = time.strftime('%Y/%m/%d %H:%M:%S', time_local)

    file_name = bt_addr.replace(':', '_') + "_temperature_file.txt"
    temperature_file_path = "/home/pi/Debug_Test/temperature_log/" + file_name
    temp_file = open(temperature_file_path, "a+")
    write_str = str(counter).zfill(4) + "\t" + dates + "\t" + str(temp_value) + '\r\n'
    temp_file.write(write_str)
    temp_file.close()


def save_accel_file(bt_addr, counter, dates, accel):
    file_name = bt_addr.replace(':', '_') + "_temperature_file.txt"
    temperature_file_path = "/home/pi/Debug_Test/temperature_log/" + file_name
    temp_file = open(temperature_file_path, "a+")
    write_str = str(counter).zfill(4) + "\t" + dates + "\t" + "accel value:" + str(accel) + "g" + '\r\n'
    temp_file.write(write_str)
    temp_file.close()


def save_light_file(bt_addr, counter, dates, state):
    file_name = bt_addr.replace(':', '_') + "_temperature_file.txt"
    temperature_file_path = "/home/pi/Debug_Test/temperature_log/" + file_name
    temp_file = open(temperature_file_path, "a+")
    write_str = str(counter).zfill(4) + "\t" + dates + "\t" + "light event:" + state + '\r\n'
    temp_file.write(write_str)
    temp_file.close()


def save_version_file(bt_addr, ver):
    file_name = bt_addr.replace(':', '_') + "_temperature_file.txt"
    temperature_file_path = "/home/pi/Debug_Test/temperature_log/" + file_name
    temp_file = open(temperature_file_path, "a+")
    write_str = "Modul Firmware Version:" + "\t" + ver + '\r\n'
    temp_file.write(write_str)
    temp_file.close()


def save_config_file(bt_addr, notif_data):
    config_temp_min = notif_data[0]
    config_temp_max = notif_data[1]

    config_temp_interval = get_mesurement_intervall(notif_data[2])
    config_accel_list = [notif_data[6], notif_data[5], notif_data[4], notif_data[3]]
    config_accel_bytearray = bytearray(config_accel_list)
    config_accel = struct.unpack("!f", config_accel_bytearray)[0]
    config_accel = round(config_accel, 3)
    config_activate = notif_data[7]

    print("config_temp_min=%d, config_temp_max=%d,interval=%d,accel=%f,activate=%d" % (
        config_temp_min, config_temp_max, config_temp_interval, config_accel, config_activate))

    file_name = bt_addr.replace(':', '_') + "_config_file.txt"
    config_file_path = "/home/pi/Debug_Test/config_files/" + file_name
    config_file = open(config_file_path, "w")

    write_str = "min:" + "\t" + str(config_temp_min) + "\t" + "max:" + "\t" + str(
        config_temp_max) + "\t" + "interval:" + "\t" + str(config_temp_interval) + "\t" + "accel:" + "\t" + str(
        config_accel) + "\t" + "activate:" + "\t" + str(config_activate)

    config_file.write(write_str)
    config_file.close()


def get_mesurement_intervall(data):
    if data == 0x01:
        intervall = 2
    elif data == 0x02:
        intervall = 5
    elif data == 0x03:
        intervall = 10
    elif data == 0x04:
        intervall = 30
    elif data == 0x05:
        intervall = 60
    elif data == 0x06:
        intervall = 60 * 2
    elif data == 0x07:
        intervall = 60 * 5
    else:
        intervall = 60 * 10

    return intervall


def show_transmission_result(bt_addr):
    global total_length, actual_length, flag_transmission_not_complete, transmission_time, lost_packet_index, counter_difference, counter_difference_old, log_counter

    print("------------------------------------------------------")
    print("-----------------Transmission Result------------------")
    print("Device Bluetooth Address: " + bt_addr)
    print("total length from Header:%d" % total_length)
    print("actual length:%d" % actual_length)
    data_loss_rate = (1 - (actual_length / total_length)) * 100
    print("data loss rate: %.4f%%" % data_loss_rate)
    print("the number of lost packet: " + str(len(lost_packet_index)))
    print("the index of lost packet: " + str(lost_packet_index))
    print("transmission time: %.4fs" % transmission_time)

    if flag_transmission_not_complete:
        print("the Transmission is not 100% completed")
    else:
        print("the Transmission is 100% completed")
    print("------------------------------------------------------")

    stat_time_int = read_start_time(bt_addr)
    time_local = time.localtime(stat_time_int)
    dates = time.strftime('%Y/%m/%d %H:%M:%S', time_local)

    file_name = bt_addr.replace(':', '_') + "_transmisson_result.txt"
    transmission_file_path = "/home/pi/Debug_Test/temperature_log/" + file_name
    transmission_file = open(transmission_file_path, "a+")
    write_str = dates + "\t" + "data loss rate: " + str(
        format(data_loss_rate, ".2f")) + "%" + "\t" + "transmission time: " + str(
        format(transmission_time, ".4f")) + "s" + "\t" + "total length: " + str(
        total_length) + "\t" + "actual length: " + str(
        actual_length) + "\t" + "flag_transmission_not_complete= " + str(
        flag_transmission_not_complete) + "\t" + '\r\n'
    transmission_file.write(write_str)
    transmission_file.close()

    file_name = bt_addr.replace(':', '_') + "_lost_packets.txt"
    lost_packet_file_path = "/home/pi/Debug_Test/temperature_log/" + file_name
    lost_packet_file = open(lost_packet_file_path, "a+")
    write_str = dates + "\t" + "the number of lost packet: " + str(
        len(lost_packet_index)) + "\t" + "index of lost packet: " + str(lost_packet_index) + '\r\n'
    lost_packet_file.write(write_str)
    lost_packet_file.close()

    total_length = 0
    actual_length = 0
    transmission_time = 0
    flag_transmission_not_complete = False
    lost_packet_index.clear()
    log_counter = 0
    counter_difference_old = 0
    counter_difference = 0


def log_blinken_test(bt_addr):
    global conn
    # global ble_addr_wil_modul1

    while True:
        try:
            print("wil_connect")
            conn = btle.Peripheral(bt_addr, btle.ADDR_TYPE_RANDOM)
            conn.setMTU(243)
            conn.setDelegate(MyDelegate())
            break
        except:
            print("!!!wil connect error!!!")
            print("!!!try again!!!")

    enable_notification()
    print("enable notification")

    # pick_by_light(5, 1)

    read_log_command = [0xc2]
    write_universal_characteristic(read_log_command)
    global notification_type
    notification_type = 1
    global notification_read_all
    notification_read_all = False
    while True:
        global timeout_counter
        if not notification_read_all and conn.waitForNotifications(1):
            continue
        if notification_read_all:
            print("get all notifications!!!")
            break
        if timeout_counter == 0:
            print("receive Notification timeout!!!")
            break
        print("timeout_counter=%d" % timeout_counter)
        timeout_counter -= 1

    pick_by_light(5, 1)
    wil_disconnect()
