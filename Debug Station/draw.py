import numpy as np
import matplotlib
import gc

matplotlib.use('AGG')
import matplotlib.pyplot as plt
from datetime import datetime
import matplotlib.dates as mdates
import os
import time
from test_setting import mesurement_time
# from test_setting import mesurement_time, WIL_Debug_Test_Addr,wil_addr_1, wil_addr_2, temp_min, temp_max

x_axis = []
y_axis = []


def data_visualization():
    wil_config = read_config_file()
    temp_min = wil_config[0]
    temp_max = wil_config[1]
    temp_interval = wil_config[2]
    tour_start_time = read_start_time()
    # tour_stop_time = read_stop_time()
    file_name = "/home/pi/bluepy/temperature_file.txt"
    tf = open(file_name, "r")
    x_axis.clear()
    y_axis.clear()
    while True:
        str = tf.readline()
        str = str.rstrip()
        if str == "":
            break
        else:
            str = str.split("\t")
            num = int(str[0])
            temp = float(str[1])

            time_int = tour_start_time + temp_interval * num
            time_local = time.localtime(time_int)
            dates = time.strftime('%Y/%m/%d/%H:%M:%S', time_local)
            xs = datetime.strptime(dates.strip(), '%Y/%m/%d/%H:%M:%S')
            x_axis.append(xs)
            y_axis.append(temp)
    tf.close()
    draw_picture(x_axis, y_axis, temp_min, temp_max, temp_interval)
    x_axis.clear()
    y_axis.clear()
    return True


def draw_picture(x, y, temp_min, temp_max, temp_interval):
    fig = plt.figure(figsize=(100, 50))
    plt.gca().xaxis.set_major_formatter(mdates.DateFormatter('%Y/%m/%d/%H:%M:%S'))

    print("temp_interval=%d" % temp_interval)
    if mesurement_time <= 60:
        plt.gca().xaxis.set_major_locator(matplotlib.dates.SecondLocator(interval=5))
    elif 60 < mesurement_time <= 10 * 60:
        plt.gca().xaxis.set_major_locator(matplotlib.dates.MinuteLocator(interval=1))
    elif 10 * 60 < mesurement_time <= 60 * 60:
        plt.gca().xaxis.set_major_locator(matplotlib.dates.MinuteLocator(interval=5))
    elif 60 * 60 < mesurement_time <= 10 * 60 * 60:
        plt.gca().xaxis.set_major_locator(matplotlib.dates.MinuteLocator(interval=30))
    elif 10 * 60 * 60 < mesurement_time < 24 * 60 * 60:
        plt.gca().xaxis.set_major_locator(matplotlib.dates.HourLocator(interval=1))
    else:
        plt.gca().xaxis.set_major_locator(matplotlib.dates.HourLocator(interval=1))

    plt.plot(x, y, linewidth=6, label="temperature")
    plt.gcf().autofmt_xdate()
    # draw 2 lins--temp min and temp max

    plt.axhline(y=temp_min, xmin=0, xmax=1, linewidth=8, color='blue', label='temp min')
    plt.axhline(y=temp_max, xmin=0, xmax=1, linewidth=8, color='red', label='temp max')
    plt.legend(loc='upper right', fontsize=60)
    plt.xticks(fontsize=50)
    plt.yticks(fontsize=50)
    plt.xlabel("Time", fontsize=60)
    plt.ylabel("Temperature(°C)", fontsize=60)
    plt.title("WIL Module Tour Log", fontsize=70)
    try:
        plt.savefig("/home/pi/bluepy/temp.jpg")
        plt.clf()
        plt.close('all')
        gc.collect()
    except:
        print("!!!!!Error plt.savefig ERROR ")
        while True:
            pass


def rename_files():
    local_time = time.time()
    dt = time.strftime("%Y-%m-%d-%H_%M_%S", time.localtime(local_time))
    file_name = "temp_" + dt + ".txt"
    pic_name = "temp" + dt + ".jpg"
    old_file_name = "/home/pi/bluepy/temperature_file.txt"
    new_file_name = "/home/pi/bluepy/" + file_name
    os.rename(old_file_name, new_file_name)
    old_pic_name = "/home/pi/bluepy/temp.jpg"
    new_pic_name = "/home/pi/bluepy/" + pic_name
    os.rename(old_pic_name, new_pic_name)

    config_file_name = "/home/pi/bluepy/config_file.txt"
    start_time_file_name = "/home/pi/Debug_Test/tour_log/start_time_file.txt"
    # stop_time_file_name = "/home/pi/bluepy/stop_time_file.txt"
    os.remove(config_file_name)
    os.remove(start_time_file_name)
    # os.remove(stop_time_file_name)


def read_config_file(bt_addr):
    wil_config = []
    file_name = bt_addr.replace(':', '_') + "_config_file.txt"
    config_file_path = "/home/pi/Debug_Test/config_files/" + file_name
    # file_name = "/home/pi/bluepy/config_file.txt"
    cf = open(config_file_path, "r")

    str = cf.readline()
    str = str.rstrip()
    if str == "":
        pass
    else:
        str = str.split("\t")
        temp_min = int(str[1])
        temp_max = int(str[3])
        temp_interval = int(str[5])
        accel = float(str[7])
        sensor_activate = int(str[9])
        # print("min=%d,max=%d,interval=%d,accel=%f,activate=%d" % (
        #     temp_min, temp_max, temp_interval, accel, sensor_activate))
        wil_config.append(temp_min)
        wil_config.append(temp_max)
        wil_config.append(temp_interval)
        wil_config.append(accel)
        wil_config.append(sensor_activate)
        # print(wil_config)
    cf.close()
    return wil_config


def read_start_time(bt_addr):
    file_name = "/home/pi/Debug_Test/tour_log/" + bt_addr.replace(':', '_') + "_start_time_file.txt"
    tf = open(file_name, "r")
    str = tf.readline()
    tf.close()
    return int(str)


def read_stop_time():
    file_name = "/home/pi/Debug_Test/tour_log/stop_time_file.txt"
    tf = open(file_name, "r")
    str = tf.readline()
    tf.close()
    return int(str)


# if __name__ == "main":
# data_visualization()
# read_config_file()
# read_start_time()
# data_visualization()

# y_axis1 = []
# y_axis2 = []
# y_axis3 = []
# y_axis4 = []
# y_axis5 = []
# y_axis6 = []
# x_axis_wil1 = []
# x_axis_wil2 = []


# def wil_laufzeittest_data(bt_addr_list):
#     global x_axis_wil1, x_axis_wil2, y_axis1, y_axis2, y_axis3, y_axis4, y_axis5, y_axis6
#     x_axis_wil1.clear()
#     x_axis_wil2.clear()
#     y_axis1.clear()
#     y_axis2.clear()
#     y_axis3.clear()
#     y_axis4.clear()
#     y_axis5.clear()
#     y_axis6.clear()
#
#     for bt_addr in bt_addr_list:
#         file_name = bt_addr.replace(':', '_') + "_temperature_file.txt"
#         temperature_file_path = "/home/pi/laufzeittest/temperature_log/" + file_name
#         tf = open(temperature_file_path, "r")
#
#         while True:
#             log_str = tf.readline()
#             log_str = log_str.rstrip()
#             if log_str == "":
#                 break
#             else:
#                 log_str = log_str.split("\t")
#                 num = int(log_str[0])
#                 log_time = str(log_str[1])
#                 temp = float(log_str[2])
#                 log_time = datetime.strptime(log_time, "%Y/%m/%d %H:%M:%S")
#
#                 if bt_addr == wil_addr_1[0]:
#                     x_axis_wil1.append(log_time)
#                     y_axis1.append(temp)
#                 elif bt_addr == wil_addr_1[1]:
#                     y_axis2.append(temp)
#                 elif bt_addr == wil_addr_1[2]:
#                     y_axis3.append(temp)
#                 elif bt_addr == wil_addr_2[0]:
#                     x_axis_wil2.append(log_time)
#                     y_axis4.append(temp)
#                 elif bt_addr == wil_addr_2[1]:
#                     y_axis5.append(temp)
#                 elif bt_addr == wil_addr_2[2]:
#                     y_axis6.append(temp)
#         tf.close()
#
#     y1_len = len(y_axis1)
#     y2_len = len(y_axis2)
#     y3_len = len(y_axis3)
#     y4_len = len(y_axis4)
#     y5_len = len(y_axis5)
#     y6_len = len(y_axis6)
#
#     y_axis_len1 = [y1_len, y2_len, y3_len]
#     y_axis_len_min1 = min(y_axis_len1)
#
#     y_axis_len2 = [y4_len, y5_len, y6_len]
#     y_axis_len_min2 = min(y_axis_len2)
#
#     for i in range(y_axis_len_min1, y1_len):
#         x_axis_wil1.pop()
#         y_axis1.pop()
#     for i in range(y_axis_len_min1, y2_len):
#         y_axis2.pop()
#     for i in range(y_axis_len_min1, y3_len):
#         y_axis3.pop()
#     for i in range(y_axis_len_min2, y4_len):
#         x_axis_wil2.pop()
#         y_axis4.pop()
#     for i in range(y_axis_len_min2, y5_len):
#         y_axis5.pop()
#     for i in range(y_axis_len_min2, y6_len):
#         y_axis6.pop()
#
#     draw_temperature(x_axis_wil1, x_axis_wil2, y_axis1, y_axis2, y_axis3, y_axis4, y_axis5, y_axis6)
#     x_axis_wil1.clear()
#     x_axis_wil2.clear()
#     y_axis1.clear()
#     y_axis2.clear()
#     y_axis3.clear()
#     y_axis4.clear()
#     y_axis5.clear()
#     y_axis6.clear()

#
# def draw_temperature(x1, x2, y1, y2, y3, y4, y5, y6):
#     fig = plt.figure(figsize=(100, 50))
#     plt.gca().xaxis.set_major_formatter(mdates.DateFormatter('%Y/%m/%d/%H:%M:%S'))
#     plt.gca().xaxis.set_major_locator(matplotlib.dates.HourLocator(interval=1))
#
#     plt.subplot(211)
#     plt.ylim(10, 30)
#     plt.plot(x1, y1, linewidth=6, label=wil_addr_1[0])
#     plt.plot(x1, y2, linewidth=6, label=wil_addr_1[1])
#     plt.plot(x1, y3, linewidth=6, label=wil_addr_1[2])
#     # plt.axhline(y=temp_min, xmin=0, xmax=1, linewidth=8, color='blue', label='temp min')
#     # plt.axhline(y=temp_max, xmin=0, xmax=1, linewidth=8, color='red', label='temp max')
#     plt.gcf().autofmt_xdate()
#     plt.legend(loc='upper right', fontsize=60)
#     plt.xticks(fontsize=50)
#     plt.yticks(fontsize=50)
#     plt.title("Szenario 1", fontsize=70)
#     plt.xlabel("Time", fontsize=60)
#     plt.ylabel("Temperature(°C)", fontsize=60)
#
#     plt.subplot(212)
#     plt.ylim(10, 30)
#     plt.plot(x2, y4, linewidth=6, label=wil_addr_2[0])
#     plt.plot(x2, y5, linewidth=6, label=wil_addr_2[1])
#     plt.plot(x2, y6, linewidth=6, label=wil_addr_2[2])
#     # plt.axhline(y=temp_min, xmin=0, xmax=1, linewidth=8, color='blue', label='temp min')
#     # plt.axhline(y=temp_max, xmin=0, xmax=1, linewidth=8, color='red', label='temp max')
#     plt.gcf().autofmt_xdate()
#     plt.legend(loc='upper right', fontsize=60)
#     plt.xticks(fontsize=50)
#     plt.yticks(fontsize=50)
#     plt.title("Szenario 2", fontsize=70)
#     plt.xlabel("Time", fontsize=60)
#     plt.ylabel("Temperature(°C)", fontsize=60)
#
#     try:
#         plt.savefig("/home/pi/laufzeittest/temperature.jpg")
#         plt.clf()
#         plt.close('all')
#         gc.collect()
#     except Exception as e:
#         print("!!!!!Error plt.savefig ERROR ")
#         print(e)
#         while True:
#             pass
#
#
# if __name__ == '__main__':
#     wil_laufzeittest_data(wil_addr_1 + wil_addr_2)
