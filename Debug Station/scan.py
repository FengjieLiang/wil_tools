from bluepy.btle import Scanner, DefaultDelegate
from test_setting import WIL_Debug_Test_Addr,mesurement_time
import time

CTC_Company_ID = "7607"
wil_module_status = dict()


class ScanDelegate(DefaultDelegate):
    def __init__(self):
        DefaultDelegate.__init__(self)

    def handleDiscovery(self, dev, isNewDev, isNewData):
        global ble_device_name
        if isNewDev:
            for (adtype, desc, value) in dev.getScanData():
                if adtype == 0x09:
                    ble_device_name = value
                    # print("ble_device_name %s" % ble_device_name)
                if adtype == 0xff:
                    company_id = value[0: 4]
                    if company_id == CTC_Company_ID:
                        global wil_module_status

                        protocol_version = int(value[4:6], 16)
                        bitset_1 = int(value[6:8], 16)
                        bitset_2 = int(value[8:10], 16)
                        error_code = int(value[10:12], 16)
                        reboot_counter = int(value[12:14], 16)
                        battery_level = int(value[14:16], 16)
                        if dev.addr in WIL_Debug_Test_Addr:
                            local_time = time.time()
                            local_time_int = int(local_time)

                            time_local = time.localtime(local_time_int)
                            dt = time.strftime("%Y-%m-%d %H:%M:%S", time_local)

                            print("")
                            print("-----------------------------------------------------------")
                            print('*****-- Time:', dt)
                            print('*****-- Device Address: %s, RSSI=%d dB' % (
                                dev.addr, dev.rssi))

                            show_module_status(protocol_version, bitset_1, bitset_2, error_code, reboot_counter,
                                               battery_level)
                            wil_module_status[dev.addr] = [dev.rssi, bitset_1, bitset_2, error_code, reboot_counter,
                                                           battery_level]

                            print("-----------------------------------------------------------")
                            print("")

        elif isNewData:
            pass


def scan_wil_module():
    scanner = Scanner().withDelegate(ScanDelegate())
    i = 0
    while True:
        try:
            scanner.scan(2.0)
            break
        except:
            i = i + 1
            print("!!!!!!scan error!!!")
            if i == 10:
                break


def show_module_status(protocol, bitset1, bitset2, error_code, reboot, battery):
    if protocol == 2:
        if bitset1 & 0x01 == 0x01:
            print("*****-- Mesurement:  on")
        else:
            print("*****-- Mesurement:  off")
        if bitset1 & 0x02 == 0x02:
            print("*****-- the data has beed read")
        else:
            print("*****-- new data!!! need read")
        if bitset1 & 0x04 == 0x04:
            print("*****-- extended sensor: Action required")
        else:
            pass
        if bitset1 & 0x08 == 0x08:
            print("*****-- extended sensor: too high")
        else:
            pass
        if bitset1 & 0x10 == 0x10:
            print("*****-- extended sensor: too low")
        else:
            pass
        if bitset1 & 0x20 == 0x20:
            print("*****-- accel sensor: Action required")
        else:
            pass
        if bitset1 & 0x40 == 0x40:
            print("*****-- accel sensor: too much accel")
        else:
            pass
        if bitset1 & 0x80 == 0x80:
            print("*****-- accel sensor: not enough accel")
        else:
            pass
        if bitset2 & 0x01 == 0x01:
            print("*****-- toggle flag: 1")
        else:
            print("*****-- toggle flag: 0")
        if bitset2 & 0x02 == 0x02:
            print("*****-- modul will go to sleep")
        # else:
        #     print("")
        if bitset2 & 0x04 == 0x04:
            print("*****-- light sensor: Action required")
        else:
            pass
        if bitset2 & 0x08 == 0x08:
            print("*****-- light sensor: too bright")
        else:
            pass
        if bitset2 & 0x10 == 0x10:
            print("*****-- light sensor: too dark")
        else:
            pass
        if bitset2 & 0x20 == 0x20:
            print("*****-- temperature sensor: Action required")
        else:
            pass
        if bitset2 & 0x40 == 0x40:
            print("*****-- temperature sensor: too hot")
        else:
            pass
        if bitset2 & 0x80 == 0x80:
            print("*****-- temperature sensor: too cold")
        else:
            pass
        print("")
        print("*************************")
        print("*****-- error code: ", error_code)
        print("*****-- reboot counter: ", reboot)
        battery_level = battery / 255 * 100
        print("*****-- battery: %d%%" % battery_level)
    else:
        print("Protocol Version error")


def save_module_status():
    local_time = time.time()
    dt = time.strftime("%Y-%m-%d  %H:%M:%S", time.localtime(local_time))
    global wil_module_status

    for key in wil_module_status:
        file_name = key.replace(':', '_') + "_adv_log.txt"
        file_path = "/home/pi/Debug_Test/adv_log/" + file_name
        adv_logfile = open(file_path, "a+")
        write_str = "time:" + "\t" + dt + "\t" + "RSSI:" + "\t" + str(
            wil_module_status[key][0]) + "\t\t" + "bitset1:" + "\t" + \
                    str(wil_module_status[key][1]) + "\t\t" + "bitset2:" + "\t" + str(
            wil_module_status[key][2]) + "\t\t" + "error code:" + "\t" + str(
            wil_module_status[key][3]) + "\t\t" + "reboot counter:" + "\t" + str(
            wil_module_status[key][4]) + "\t\t" + "battery level" + "\t" + str(wil_module_status[key][5]) + "%" + "\r\n"
        adv_logfile.write(write_str)
        adv_logfile.close()


def scan_and_wait(waiting_time):
    for i in range(int(mesurement_time / 5)):
        time.sleep(3)
        scan_wil_module()
        if i % (waiting_time / 5) == 0:
            save_module_status()


if __name__ == '__main__':
    scan_wil_module()
