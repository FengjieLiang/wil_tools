import scan
import connect
import time
import struct
from test_setting import WIL_Debug_Test_Addr, temp_intervall_10s, temp_intervall_1m, \
    temp_intervall_2s, temp_intervall_5m, WIL_test_module, wil_test_module_addr_6


def debug_test():
    # while True:
        print('Debug Test for WIL Module')
        time.sleep(1)

        print("start scanning for 2s")
        scan.scan_wil_module()

        wil_start_tour(WIL_Debug_Test_Addr, temp_intervall_2s)

        scan.scan_and_wait(5 * 60)

        wil_stop_tour(WIL_Debug_Test_Addr)

        wil_read_log(WIL_Debug_Test_Addr)
        time.sleep(5)


def wil_start_tour(test_device_addr, temp_intervall):
    for wil_device in test_device_addr:
        print("start tour : %s" % wil_device)

        print("connecting wil modul")
        connect.wil_connect(wil_device)
        # time.sleep(2)

        print("config sensor")
        connect.wil_sensor_config(temp_intervall)
        time.sleep(1)

        print("start tour")
        connect.wil_start_tour(wil_device)
        time.sleep(1)

        print("LED flashing")
        connect.pick_by_light(5, 1)
        time.sleep(1)

        print("disconnecting wil modul")
        connect.wil_disconnect()
        # time.sleep(3)


def wil_stop_tour(test_device_addr):
    for wil_device in test_device_addr:
        print("stop tour : %s" % wil_device)

        print("connecting wil modul")
        connect.wil_connect(wil_device)
        time.sleep(1)

        print("stop tour")
        connect.wil_stop_tour(wil_device)
        time.sleep(1)

        print("disconnecting wil modul")
        connect.wil_disconnect()
        time.sleep(1)


def wil_read_log(test_device_addr):
    for wil_device in test_device_addr:
        print("read tour log: %s" % wil_device)
        local_time = time.time()
        local_time_int = int(local_time)
        time_local = time.localtime(local_time_int)
        dt = time.strftime("%Y-%m-%d %H:%M:%S", time_local)
        print('***Time: ', dt)

        print("connecting wil modul")
        connect.wil_connect(wil_device)
        # time.sleep(2)

        print("read config")
        connect.wil_read_sensor_config(wil_device)
        # time.sleep(2)

        print("read log")
        if connect.wil_read_log(wil_device):
            time.sleep(2)

        print("disconnecting wil modul")
        connect.wil_disconnect()
        time.sleep(1)

        connect.show_transmission_result(wil_device)
        time.sleep(2)


if __name__ == '__main__':
    debug_test()

    # print("connecting wil modul")
    # connect.wil_connect(wil_test_module_addr_6)
    # time.sleep(2)
    #
    # connect.wil_stop_tour(wil_test_module_addr_6)
    #
    # print("read config")
    # connect.wil_read_sensor_config(wil_test_module_addr_6)
    # time.sleep(2)
    #
    # print("read log")
    # if connect.wil_read_log(wil_test_module_addr_6):
    #     time.sleep(2)
    #
    # print("disconnecting wil modul")
    # connect.wil_disconnect()
    # time.sleep(1)
    #
    # connect.show_transmission_result(wil_test_module_addr_6)
    # time.sleep(2)
