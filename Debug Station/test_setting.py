# wil_test_module_addr_1 = 'ed:b5:db:9b:aa:d8'
# wil_test_module_addr_1 = 'cf:73:24:45:0b:48'
wil_test_module_addr_2 = 'e3:76:90:47:41:ea'
# wil_test_module_addr_3 = 'c8:54:89:80:bf:fc'
# wil_test_module_addr_4 = 'fd:21:d7:9f:1e:0f'
# wil_test_module_addr_5 = 'c8:1b:bf:6c:dc:fb'
wil_test_module_addr_6 = 'e9:57:a4:ea:6d:b5'
WIL_Debug_Test_Addr = [wil_test_module_addr_2]

WIL_test_module = 'CB:65:37:8D:AE:D0'

mesurement_time = 10 * 60
temp_min = 15
temp_max = 30

acceleration = 1.7

temp_min_enable = 1
temp_max_enable = 1
acceleration_enable = 1
light_enable = 1

# 0x01->2s
# 0x02->5s
# 0x03->10s
# 0x04->30s
# 0x05->1min
# 0x06->2min
# 0x07->5min
# 0x08->10min
# temp_interval = 4

temp_intervall_2s = 0x01
temp_intervall_5s = 0x02
temp_intervall_10s = 0x03
temp_intervall_30s = 0x04
temp_intervall_1m = 0x05
temp_intervall_2m = 0x06
temp_intervall_5m = 0x07
temp_intervall_10m = 0x08
